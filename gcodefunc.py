def encoding(message):
    sendarray = bytearray()
    sendarray.append(0x1A)
    sendarray.append(0x20 +len(message))
    sendarray.extend(message.encode('utf-8'))
    sendarray.append(0x0A)
    return sendarray