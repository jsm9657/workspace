import socket
import time
import sys
import binascii
from gcodefunc import *

HOST = '192.168.0.191'
PORT = 20000
BUFSIZE = 1024
 
# Open grbl serial port
s = socket.socket() 

s.connect((HOST, PORT)) 

s.send(encoding('M73'))

s.close