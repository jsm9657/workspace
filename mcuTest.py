import socket
import time
import sys
import binascii
import errno
import os
from threading import Thread


class GlobalObject:
    clientSocket = None
    clientAddr = None


#HOSTS = '192.168.1.195'
HOSTS = '192.168.1.145'
PORTS = 20000
BUFSIZE = 1024

server = socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((HOSTS, PORTS))
server.listen()
GlobalObject.clientSocket, GlobalObject.clientAddr = server.accept()


def recive(GlobalObject):
    while True:
        try:
            temp = bytearray()
            while True:
                bit = GlobalObject.clientSocket.recv(1)
                temp.append(bit[0])
                if bit[0] == 0x0A:
                    break
            if temp == bytearray(b'\x1a"\x1bj\n'):
                pass
            else:
                print(temp)
                time.sleep(0.01)
                if 'M316' in str(temp):
                    GlobalObject.clientSocket.send(bytes(
                        [0x01, 0x03, 0x10, 0x0F, 0x36, 0x01, 0x2B, 0x01, 0x31, 0x01, 0x26, 0x01, 0x2A, 0x01, 0x2B, 0x02, 0x88, 0x01, 0x4A, 0xFF]))
                    print('dispenser\n')
                if temp == bytearray(b'\x03\n'):
                    pass
                else:
                    pass
                    GlobalObject.clientSocket.send("qq\n".encode())
                    print('qq\n')
        except:
            server = socket.socket()
            server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server.bind((HOSTS, PORTS))
            server.listen()
            GlobalObject.clientSocket, GlobalObject.clientAddr = server.accept()


def send(GlobalObject):
    while True:
        sendqq = input()
        if sendqq == '':
            GlobalObject.clientSocket.send("qq\n".encode())
            print('qq\n')
        elif sendqq == '1':
            GlobalObject.clientSocket.send(
                bytes([0x03, 0x03, 0x02, 0x00, 0xFC, 0xB8, 0x05]))
        elif sendqq == '2':
            GlobalObject.clientSocket.send(
                bytes([0x03, 0x03, 0x02, 0x01, 0x74, 0xB8, 0x05]))
        elif sendqq == '3':
            GlobalObject.clientSocket.send(
                bytes([0x03, 0x03, 0x02, 0x01, 0x34, 0xB8, 0x05]))
        elif sendqq == '4':
            GlobalObject.clientSocket.send(bytes(
                [0x01, 0x03, 0x10, 0x0F, 0x36, 0x01, 0x2B, 0x01, 0x31, 0x01, 0x26, 0x01, 0x2A, 0x01, 0x2B, 0x02, 0x88, 0x01, 0x4A, 0xFF]))
        elif sendqq == '5':
            GlobalObject.clientSocket.send('*err: 22\n'.encode())
        elif sendqq == '6':
            GlobalObject.clientSocket.send('*err: 23\n'.encode())
        else:
            time.sleep(0.01)
            GlobalObject.clientSocket.send(sendqq.encode())
            print(sendqq)
        print(sendqq)


th1 = Thread(target=recive, args=(GlobalObject,))
th2 = Thread(target=send, args=(GlobalObject,))
th1.start()
th2.start()
