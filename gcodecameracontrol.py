#!/usr/bin/env python
"""/
Simple g-code streaming script for grbl
"""

import socket
import sys
import os
import errno
from threading import Thread

HOSTS = '192.168.10.36'
PORTS = 22000

server = socket.socket()
server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
server.bind((HOSTS,PORTS))


class GlobalObject():
    client = None
    addr = None


def ConnectDevieceSocket(GlobalObject):
    server.listen()
    GlobalObject.client, GlobalObject.addr = server.accept()
    GlobalObject.client.settimeout(5)
    thZoom = Thread(target=CameraZoomValueReciver, args=(GlobalObject,))
    thZoom.start()

def CameraZoomValueReciver(GlobalObject):
    while True:
        try:
            temp = GlobalObject.client.recv(4)
            if len(temp) == 0 :
                raise Exception
            cameraZoomValue = int.from_bytes(temp, byteorder ='big', signed=True)
            if cameraZoomValue == 10 :
                print('포커스')
            elif cameraZoomValue>=100 and cameraZoomValue<=800:
                cameraZoomControl = f"v4l2-ctl -d 2 -c zoom_absolute={cameraZoomValue}"  
                print(cameraZoomControl)
                #os.system(cameraZoomControl)
            else :
                print(cameraZoomValue)
        except ConnectionError:
            GlobalObject.client.close()
            thConn = Thread(target=ConnectCamSocket, args=(GlobalObject,))
            print('연결해제')
            thConn.start()
            break
        except socket.error:
            GlobalObject.client.close()
            thConn = Thread(target=ConnectCamSocket, args=(GlobalObject,))
            print('타임아웃')
            thConn.start()
            break
        except Exception:
            GlobalObject.client.close()
            thConn = Thread(target=ConnectCamSocket, args=(GlobalObject,))
            print('오류')
            thConn.start()
            break

ConnectDevieceSocket(GlobalObject)


