#!/usr/bin/env python
"""\
Simple g-code streaming script for grbl
"""
 
import socket
import time
import sys
import binascii
import errno
import os
from threading import Thread

#HOST = '192.168.0.191'
#HOST = '192.168.10.20'
HOST = '192.168.1.195'
PORT = 20000
BUFSIZE = 1024


#HOSTS = '192.168.10.36'
HOSTS = '192.168.1.1'
PORTS = 21000 

os.chdir('/home/pi/gcodefile')

class GlobalObject():
    sendMode = 'Nomal'
    status = 'READY'
    qq = False
    Msocketlife = False
    current_line = 0
    client = None #mobile
    addr = None #mobile
    Msocket = socket.socket()
    home = ['M29 Y', 'M29 X','M29 Z', 'M29 C','M29 A','G78 B50.','M29 B']
    home_status = 'PLAY'
    Real_temp = False
    gcodeFile = None
    line = None


def ConnectDevieceSocket():
    server.listen()
    GlobalObject.client, GlobalObject.addr = server.accept()
    print(GlobalObject.addr)
    GlobalObject.client.setsockopt(socket.SOL_SOCKET,socket.SO_KEEPALIVE,1)
    GlobalObject.client.setsockopt(socket.SOL_TCP,socket.TCP_KEEPIDLE,30)
    GlobalObject.client.setsockopt(socket.SOL_TCP,socket.TCP_KEEPINTVL,5)
    GlobalObject.client.setsockopt(socket.SOL_TCP,socket.TCP_KEEPCNT,3)

# Open grbl serial port
def ConnectMcuSocket() :
    if not GlobalObject.Msocketlife :
        try :
            GlobalObject.Msocket.connect((HOST, PORT)) 
            GlobalObject.Msocketlife = True
            th2.start()
        except :
            GlobalObject.client.send('MCU not connect\n'.encode())
            print('MCU not connect')
            GlobalObject.Msocketlife = False

def encoding(message):
    sendarray = bytearray()
    sendarray.append(0x1A)
    sendarray.append(0x20 +len(message))
    sendarray.extend(message.encode('utf-8'))
    sendarray.append(0x0A)
    return sendarray

def deviceReceive(GlobalObject):
    filename = 'temp'
    while True :
        try:
            temp = bytearray()
            while True:
                bit = GlobalObject.client.recv(1)
                temp.append(bit[0])
                if bit[0] == 0x0A:
                    break
            if GlobalObject.sendMode == 'Nomal':
                if temp.decode('ascii') == 'FileName\n':
                    filenameByte = bytearray()
                    while True:
                        bit = GlobalObject.client.recv(1)
                        filenameByte.append(bit[0])
                        if bit[0] == 0x0A:
                            break
                    filename = filenameByte.decode('ascii').strip()
                    print(temp.decode('ascii'))
                    print(filename)
                elif temp.decode('ascii') == 'START\n':
                    if filename == 'temp':
                        GlobalObject.client.send('no file\n'.encode())
                    else:
                        #파일 존재여부 확인 필요
                        GlobalObject.sendMode = 'File'
                        GlobalObject.status = 'START'
                        th3 = Thread(target=GcodeFileSend,
                                     args=(GlobalObject, filename))
                        th3.start()
                elif temp.decode('ascii') == 'HOME\n':
                    GlobalObject.sendMode = 'HOME'
                    th4 = Thread(target=Home, args=(GlobalObject,))
                    th4.start()
                else:
                    print(temp.decode())
                    GlobalObject.Msocket.send(temp)
            elif GlobalObject.sendMode == 'File':
                if temp == encoding('M316'):
                    GlobalObject.Real_temp = True
                status = temp.decode('ascii')
                if status == 'START\n' :
                    GlobalObject.status = 'START'
                    GlobalObject.Msocket.send(bytearray([2,0x0A]))
                elif status == 'PAUSE\n' :
                    GlobalObject.status ='PAUSE'
                    GlobalObject.Msocket.send(bytearray([1,0x0A]))        
                elif status == 'STOP\n' :
                    GlobalObject.status ='STOP'
                    GlobalObject.Msocket.send(bytearray([3,0x0A]))
                    GcodeFileEnd(GlobalObject)
            elif GlobalObject.sendMode == 'HOME':
                try :
                    status = temp.decode('ascii')
                    if status == 'STOP\n' :
                        GlobalObject.status = 'STOP'
                        GlobalObject.home_status = 'STOP'
                        GlobalObject.Msocket.send(bytearray([3,0x0A]))
                except :
                    pass
        except ConnectionResetError :
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()
        except OSError:
            try:
                GlobalObject.client.send('retry please\n'.encode())
                print('retry please')
            except :
                GlobalObject.client.close()
                ConnectDevieceSocket()
                ConnectMcuSocket()
        except Exception :
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()
            


def Home(GlobalObject):
    time.sleep(0.1)
    GlobalObject.home_status = 'PLAY'
    breakflag = False
    for i in GlobalObject.home :
        if breakflag :
            break
        GlobalObject.qq = False
        GlobalObject.Msocket.send(encoding(i))
        while True :
            if GlobalObject.home_status =='PLAY' :
                if GlobalObject.qq :
                    break
            elif GlobalObject.home_status == 'STOP' :
                breakflag = True
                break
    GlobalObject.qq = True
    GlobalObject.sendMode ='Nomal'


def McuReceive(GlobalObject):
    while GlobalObject.Msocketlife:
        try :
            temp = bytearray()
            bit = GlobalObject.Msocket.recv(1)
            temp.append(bit[0])
            if bit[0] == 0x01:
                #dispenser 데이터 전송
                print('dispenser Data')
                bit = GlobalObject.Msocket.recv(2)
                temp.extend(bit)
                bit = GlobalObject.Msocket.recv(temp[2]+1)
                temp.extend(bit)
                bit = GlobalObject.Msocket.recv(1)
                try:
                    GlobalObject.client.send(temp)
                    print('dispenser Data send Device')
                except:
                    print('Not open client socket')
            else:
                while True :
                    bit = GlobalObject.Msocket.recv(1)
                    temp.append(bit[0])
                    if bit[0] == 0x0A : break
                try:
                    if temp.decode('ascii') == 'qq\n':
                        GlobalObject.qq = True
                        print(temp.decode('ascii'))
                        try:
                            if GlobalObject.sendMode == 'File' :
                                sendline = 'line:%i\n' % GlobalObject.current_line
                                GlobalObject.client.send(sendline.encode())
                                th5 = Thread(target=sendLine, args=(GlobalObject,))
                                th5.start()
                        except :
                            pass
                    elif temp.decode('ascii') == 'RR\n':
                        try:
                            if GlobalObject.sendMode == 'File' :                    
                                th5 = Thread(target=reSendLine, args=(GlobalObject,))
                                th5.start()
                        except :
                            pass
                    else :
                        #print(temp)
                        pass
                except :
                    #print(temp)
                    pass
                try:
                    GlobalObject.client.send(temp)
                except:
                    print('Not open client socket')
        except socket.timeout :
            pass
        except socket.error :
            print('Gsocket error')
            GlobalObject.Msocket = socket.socket()
            try:
                GlobalObject.Msocket.connect((HOST, PORT))
            except:
                GlobalObject.client.send('except MCU\n'.encode())
                GlobalObject.Msocketlife =False
                break
            print('except MCU')


def sendLine(GlobalObject):
    if GlobalObject.gcodeFile is None :
        print('Not open file')
        return
    while True :
        GlobalObject.line = GlobalObject.gcodeFile.readline()
        #EOL 체크 확인 필요
        if not GlobalObject.line :
            GcodeFileEnd(GlobalObject)
            break
        else :
            l = GlobalObject.line.strip()
            if len(l) != 0 and l[0] != '#' and l[0] != ';' and l[0] != '(': #real send line
                print(l)
                GlobalObject.qq = False
                GlobalObject.Msocket.send(encoding(l)) # Send g-code block to grbl
                GlobalObject.current_line += 1
                print(GlobalObject.current_line)
                break

def reSendLine(GlobalObject):
    l = GlobalObject.line.strip()
    GlobalObject.qq = False
    GlobalObject.Msocket.send(encoding(l))

def GcodeFileEnd(GlobalObject):
    GlobalObject.gcodeFile.close()
    GlobalObject.gcodeFile = None
    GlobalObject.line = None
    GlobalObject.current_line = 0
    GlobalObject.qq = False
    GlobalObject.status = 'READY'
    GlobalObject.sendMode = 'Nomal'

    

def GcodeFileSend(GlobalObject, gcodeFileName):
    time.sleep(2)
    # Open g-code file
    bomf = open(gcodeFileName,'r',encoding='utf-8-sig').read()
    open(gcodeFileName,'w',encoding='utf-8').write(bomf)
    GlobalObject.gcodeFile = open(gcodeFileName,'r')

    th5 = Thread(target=sendLine, args=(GlobalObject,))
    th5.start()




    # countr = 0
    # breakflag = False
    # for line in f:
    #     if breakflag:
    #         break 
    #     l = line.strip() # Strip all EOL characters for streaming
    #     if len(l) != 0 and l[0] != '#' and l[0] != ';' and l[0] != '(': #real send line
    #         print(l)
    #         GlobalObject.qq = False
    #         GlobalObject.Msocket.send(encoding(l)) # Send g-code block to grbl
    #         GlobalObject.current_line += 1
    #         print(GlobalObject.current_line)
    #         while True: # Wait qq and Check Status
    #             if GlobalObject.status == 'START':
    #                 if GlobalObject.qq :
    #                     countr +=1
    #                     break
    #             elif GlobalObject.status == 'PAUSE':
    #                 pass
    #             elif GlobalObject.status == 'STOP':
    #                 breakflag = True
    #                 break
        # if GlobalObject.Real_temp :
        #     print('Real Temp!')
        #     GlobalObject.qq = False
        #     GlobalObject.Msocket.send(encoding('M316'))
        #     while True: # Wait qq and Check Status
        #         if GlobalObject.status == 'START':
        #             if GlobalObject.qq :
        #                 GlobalObject.Real_temp = False
        #                 break
        #         elif GlobalObject.status == 'PAUSE':
        #             pass
        #         elif GlobalObject.status == 'STOP':
        #             breakflag = True
        #             break
    # Wait here until grbl is finished to close serial port and file.
    # print('line :',GlobalObject.current_line )
    # print('countqq :',countr)
    # f.close()
    # GlobalObject.current_line = 0
    # GlobalObject.qq = True
    # GlobalObject.status = 'READY'
    # GlobalObject.sendMode = 'Nomal'

server = socket.socket()
server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
server.bind((HOSTS,PORTS))
th1 = Thread(target=deviceReceive, args=(GlobalObject,), daemon=True)
th2 = Thread(target=McuReceive, args=(GlobalObject,), daemon=True)
ConnectDevieceSocket()
ConnectMcuSocket()
th1.start()
th1.join()
th2.join()

