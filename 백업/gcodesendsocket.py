#!/usr/bin/env python
"""\
Simple g-code streaming script for grbl
"""

import socket
import time
import sys
import binascii
import errno
import os
from threading import Thread

#MCU socket ip, port
HOST = '192.168.0.31'
PORT = 20000
BUFSIZE = 1024

#Cam socket ip port
HOSTCAM = '192.168.1.1'
PORTCAM = 22000

#Device socket ip port
HOSTS = '192.168.1.1'
PORTS = 21000

#작업 공간 변경
os.chdir('/home/pi/gcodefile')

#상수 정의
CONST_SENDMODE_NOMAL = 'NOMAL'
CONST_SENDMODE_HOME = 'HOME'
CONST_SENDMODE_FILE = 'FILE'
CONST_SENDMODE_FILESTATUS = 'FILESTATUS'
CONST_SENDMODE_FILEDISPENSER = 'FILEDISPENSER'

CONST_STATUS_READY = 'READY'
CONST_STATUS_START = 'START'
CONST_STATUS_PAUSE = 'PAUSE'
CONST_STATUS_STOP = 'STOP'

#Global 변수 클래스
class GlobalObject():
    send_mode = CONST_SENDMODE_NOMAL #현재 보내기 모드(NOMAL FILE FILEDISPENSER FILESTATUS HOME)
    status = CONST_STATUS_READY # 현재 출력 상태(READY는 맨 처음 START PAUSE STOP)
    qq_flag = True #qq 상태 명령어 보낸후 qq 응답 받기 전까지 False
    mcu_socket_life = False #재 접속시 MCU 소켓이 살아있는지 확인
    client = None  # mobile
    addr = None  # mobile
    client_cam = None  # camera
    addr_cam = None  # camera
    mcu_socket = socket.socket()
    gcode_file = None #g code file path
    gcode_file_name = 'NoFile'
    gcode_file_totla_line = 0
    current_gcodefile_command = None #현재 gcode file 커맨드
    current_line = 0 #현재 gcode file line 번호
    home_command = ['M29 Y', 'M29 X', 'M29 Z', 'M29 C', 'G78 B50. F300',  'M29 B', 'G79', 'M29 A']
    home_flag = 0 #home command index
    status_command = ['M316', 'M356', 'M358', 'M359']
    status_command_flag = 0 #status command index
    dispenser_get_command = ['M311', 'M312',
                           'M313', 'M314', 'M315', 'M317', 'M318']
    dispenser_get_command_flag = 0 #dispenser command index

#cam socket server listen
def ConnectCamSocket():
    serverCam.listen()
    GlobalObject.client_cam, GlobalObject.addr_cam = serverCam.accept()
    thZoom = Thread(target=CameraValueReciver, args=())
    thZoom.start()

#Cam 포커스 및 줌 server on off
def CameraValueReciver():
    while True:
        try:
            temp = GlobalObject.client_cam.recv(4)
            if len(temp) == 0:
                raise Exception
            cameraValue = int.from_bytes(
                temp, byteorder='big', signed=True)
            if cameraValue == 10:
                print('auto on')
                camMessage = 'v4l2-ctl -d 2 -c focus_auto=0'
                os.system(camMessage)
                print('auto off')
                camMessage = 'v4l2-ctl -d 2 -c focus_auto=1'
                os.system(camMessage)
            elif cameraValue >= 100 and cameraValue <= 800:
                camMessage = 'v4l2-ctl -d 2 -c zoom_absolute=' + \
                    str(cameraValue)
                os.system(camMessage)
            elif cameraValue == 801:
                th10 = Thread(target=StartFFserver, args=())
                th10.start()
            elif cameraValue == 802:
                print('802sucssesdasd')
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
            elif cameraValue == 803:
                th12 = Thread(target=StartFFserver2, args=())
                th12.start()
            elif cameraValue == 804:
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
        except ConnectionError:
            GlobalObject.client_cam.close()
            thConn = Thread(target=ConnectCamSocket)
            print('연결해제')
            thConn.start()
            break
        except socket.error:
            GlobalObject.client_cam.close()
            thConn = Thread(target=ConnectCamSocket)
            print('타임아웃')
            thConn.start()
            break
        except Exception as e:
            GlobalObject.client_cam.close()
            thConn = Thread(target=ConnectCamSocket)
            print(e)
            thConn.start()
            break

def StartFFserver():
    EX = "ffserver -f ffserver.conf & ffmpeg -f v4l2 -s 640x480 -r 30 -i /dev/video0 http://localhost:8090/feed1.ffm"
    os.system(EX)
def FFserverKill():
    EX = 'pkill ffserver'
    os.system(EX)
def StartFFserver2():
    EX = "ffserver -f ffserver1.conf & ffmpeg -f v4l2 -s 1280x720 -r 16 -i /dev/video2 http://localhost:8092/feed2.ffm"
    os.system(EX)

def ConnectDevieceSocket():
    server.listen()
    GlobalObject.client, GlobalObject.addr = server.accept()
    print(GlobalObject.addr)
    GlobalObject.client.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 30)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 5)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 3)
    

def ConnectMcuSocket():
    if not GlobalObject.mcu_socket_life:
        try:
            GlobalObject.mcu_socket.connect((HOST, PORT))
            GlobalObject.mcu_socket_life = True
            th2 = Thread(target=McuReceiver, args=(), daemon=True)
            th2.start()
        except:
            GlobalObject.client.send('MCU not connect\n'.encode())
            print('MCU not connect')
            GlobalObject.mcu_socket_life = False

#MUC 통신 프로토콜
def encoding(message):
    sendarray = bytearray()
    sendarray.append(0x1A)
    sendarray.append(0x20 + len(message))
    sendarray.extend(message.encode('utf-8'))
    sendarray.append(0x0A)
    return sendarray

def DeviceReceiver():
    while True:
        try:
            temp = bytearray()
            while True:
                bit = GlobalObject.client.recv(1)
                temp.append(bit[0])
                if bit[0] == 0x0A:
                    break
            if GlobalObject.send_mode == CONST_SENDMODE_NOMAL:
                if temp.decode('ascii') == 'FileName\n':
                    filenameByte = bytearray()
                    while True:
                        bit = GlobalObject.client.recv(1)
                        filenameByte.append(bit[0])
                        if bit[0] == 0x0A:
                            break
                    GlobalObject.gcode_file_name = filenameByte.decode('ascii').strip()
                    print(temp.decode('ascii'))
                    print(GlobalObject.gcode_file_name)
                elif temp.decode('ascii') == 'START\n':
                    if GlobalObject.gcode_file_name == 'NoFile':
                        GlobalObject.client.send('no file\n'.encode())
                    else:
                        # 파일 존재여부 확인 필요
                        if os.path.isfile(GlobalObject.gcode_file_name) :
                            GlobalObject.send_mode = CONST_SENDMODE_FILE
                            GlobalObject.status = CONST_STATUS_START
                            th3 = Thread(target=GcodeFileSender,
                                        args=())
                            th3.start()
                        else : 
                            print("No File")
                            GlobalObject.client.send('no file\n'.encode())
                elif temp.decode('ascii') == 'HOME\n':
                    GlobalObject.send_mode = CONST_SENDMODE_HOME
                    th4 = Thread(target=Home, args=())
                    th4.start()
                elif temp.decode('ascii') == 'STOP\n':
                    GlobalObject.mcu_socket.send(bytearray([3, 0x0A]))
                elif temp.decode('ascii') == 'PAUSE\n':
                    pass
                else:
                    print(temp.decode())
                    GlobalObject.mcu_socket.send(temp)
            elif GlobalObject.send_mode == CONST_SENDMODE_FILE or GlobalObject.send_mode == CONST_SENDMODE_FILESTATUS or GlobalObject.send_mode == CONST_SENDMODE_FILEDISPENSER:
                status = temp.decode('ascii')
                if status == 'START\n':
                    GlobalObject.status = CONST_STATUS_START
                    GlobalObject.mcu_socket.send(bytearray([2, 0x0A]))
                elif status == 'PAUSE\n':
                    GlobalObject.status = CONST_STATUS_PAUSE
                    GlobalObject.mcu_socket.send(bytearray([1, 0x0A]))
                elif status == 'STOP\n':
                    GlobalObject.status = CONST_STATUS_STOP
                    GlobalObject.mcu_socket.send(bytearray([3, 0x0A]))
                    GcodeFileEnd()
            elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                try:
                    status = temp.decode('ascii')
                    if status == 'STOP\n':
                        GlobalObject.status = CONST_STATUS_STOP
                        GlobalObject.mcu_socket.send(bytearray([3, 0x0A]))
                        GlobalObject.send_mode = CONST_SENDMODE_NOMAL
                except:
                    pass
        except ConnectionResetError:
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()
        except OSError:
            try:
                GlobalObject.client.send('retry please\n'.encode())
                print('retry please')
            except:
                GlobalObject.client.close()
                ConnectDevieceSocket()
                ConnectMcuSocket()
        except Exception:
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()

def Home():
    time.sleep(0.1)
    homecom = GlobalObject.home_command[GlobalObject.home_flag]
    GlobalObject.qq_flag = False
    GlobalObject.mcu_socket.send(encoding(homecom))
    GlobalObject.home_flag += 1
    if(GlobalObject.home_flag == len(GlobalObject.home_command)):
        GlobalObject.home_flag = 0
        GlobalObject.send_mode = CONST_SENDMODE_NOMAL
    print('Send home Mcommand')

def McuReceiver():
    while GlobalObject.mcu_socket_life:
        try:
            temp = bytearray()
            bit = GlobalObject.mcu_socket.recv(1)
            temp.append(bit[0])
            if bit[0] == 0x01:
                # dispenser 데이터 전송
                print('dispenser Data')
                bit = GlobalObject.mcu_socket.recv(2)
                temp.extend(bit)
                if temp[2] == 0x02:
                    bit = GlobalObject.mcu_socket.recv(4)
                    temp.extend(bit)
                    bit = GlobalObject.mcu_socket.recv(1)
                else:
                    bit = GlobalObject.mcu_socket.recv(temp[2]+1)
                    temp.extend(bit)
                    bit = GlobalObject.mcu_socket.recv(1)
                try:
                    GlobalObject.client.send(temp)
                    print('dispenser Data send Device')
                except:
                    print('Not open client socket')
            else:
                while True:
                    bit = GlobalObject.mcu_socket.recv(1)
                    temp.append(bit[0])
                    if bit[0] == 0x0A:
                        break
                try:
                    if temp.decode('ascii') == 'qq\n':
                        GlobalObject.qq_flag = True
                        print(temp.decode('ascii'))
                        try:
                            if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                                sendline = 'line:%i\n' % GlobalObject.current_line
                                GlobalObject.client.send(sendline.encode())
                                th5 = Thread(target=sendLine,
                                             args=())
                                th5.start()
                            elif GlobalObject.send_mode == CONST_SENDMODE_FILESTATUS:
                                th6 = Thread(target=sendStatus,
                                             args=())
                                th6.start()
                            elif GlobalObject.send_mode == CONST_SENDMODE_FILEDISPENSER:
                                th6 = Thread(target=sendDispenser,
                                             args=())
                                th6.start()
                            elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                                th4 = Thread(target=Home, args=())
                                th4.start()
                        except:
                            pass
                    elif temp.decode('ascii') == 'RR\n':
                        try:
                            if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                                th5 = Thread(target=reSendLine,
                                             args=())
                                th5.start()
                        except:
                            pass
                    else:
                        # print(temp)
                        pass
                except:
                    # print(temp)
                    pass
                try:
                    GlobalObject.client.send(temp)
                except:
                    print('Not open client socket')
        except socket.timeout:
            pass
        except socket.error:
            print('Gsocket error')
            GlobalObject.mcu_socket = socket.socket()
            try:
                GlobalObject.mcu_socket.connect((HOST, PORT))
            except:
                GlobalObject.client.send('except MCU\n'.encode())
                GlobalObject.mcu_socket_life = False
                break
            print('except MCU')

def sendLine():
    if GlobalObject.gcode_file is None:
        print('Not open file')
        return
    while True:
        GlobalObject.current_gcodefile_command = GlobalObject.gcode_file.readline()
        # EOL 체크 확인 필요
        if not GlobalObject.current_gcodefile_command:
            GcodeFileEnd()
            break
        else:
            raw_command = GlobalObject.current_gcodefile_command.strip()
            if len(raw_command) != 0 and raw_command[0] != '#' and raw_command[0] != ';' and raw_command[0] != '(':  # real send line
                send_command_line = raw_command.split(';')[0].strip()
                print(send_command_line)
                GlobalObject.qq_flag = False
                if 'M308' in send_command_line :
                    GlobalObject.send_mode = CONST_SENDMODE_FILEDISPENSER
                # Send g-code block to grbl
                GlobalObject.mcu_socket.send(encoding(send_command_line))
                GlobalObject.client.send((send_command_line+'\n').encode())
                GlobalObject.current_line += 1
                print(GlobalObject.current_line)
                break
            elif raw_command.startswith(';LAYER:'):
                GlobalObject.send_mode = CONST_SENDMODE_FILESTATUS
                th6 = Thread(target=sendStatus, args=())
                th6.start()
                break

def sendStatus():
    statuscom = GlobalObject.status_command[GlobalObject.status_command_flag]
    GlobalObject.qq_flag = False
    GlobalObject.status_command_flag += 1
    if(GlobalObject.status_command_flag == len(GlobalObject.status_command)):
        GlobalObject.status_command_flag = 0
        GlobalObject.send_mode = CONST_SENDMODE_FILE
    GlobalObject.mcu_socket.send(encoding(statuscom))
    GlobalObject.client.send((statuscom+'\n').encode())
    print('Send Status Mcommand : ', statuscom)

def sendDispenser():
    dispensercom = GlobalObject.dispenser_get_command[GlobalObject.dispenser_get_command_flag]
    GlobalObject.qq_flag = False
    GlobalObject.dispenser_get_command_flag += 1
    if(GlobalObject.dispenser_get_command_flag == len(GlobalObject.dispenser_get_command)):
        GlobalObject.dispenser_get_command_flag = 0
        GlobalObject.send_mode = CONST_SENDMODE_FILE
    GlobalObject.mcu_socket.send(encoding(dispensercom))
    GlobalObject.client.send((dispensercom+'\n').encode())
    print('Send Dispenser Mcommand : ' , dispensercom)

def reSendLine():
    l = GlobalObject.current_gcodefile_command.strip()
    GlobalObject.qq_flag = False
    GlobalObject.mcu_socket.send(encoding(l))

def GcodeFileEnd():
    GlobalObject.mcu_socket.send(encoding('M330'))
    GlobalObject.gcode_file.close()
    GlobalObject.gcode_file = None
    GlobalObject.current_gcodefile_command = None
    GlobalObject.current_line = 0
    GlobalObject.qq_flag = False
    GlobalObject.status_command_flag = 0
    GlobalObject.dispenser_get_command_flag = 0
    GlobalObject.status = CONST_STATUS_READY
    GlobalObject.send_mode = CONST_SENDMODE_NOMAL

def GcodeFileSender():
    time.sleep(2)
    # Open g-code file
    bomf = open(GlobalObject.gcode_file_name, 'r', encoding='utf-8-sig').read()
    open(GlobalObject.gcode_file_name, 'w', encoding='utf-8').write(bomf)
    GlobalObject.gcode_file = open(GlobalObject.gcode_file_name, 'r')
    th5 = Thread(target=sendLine, args=())
    th5.start()
    
#init
server = socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((HOSTS, PORTS))

serverCam = socket.socket()
serverCam.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serverCam.bind((HOSTCAM, PORTCAM))

th1 = Thread(target=DeviceReceiver, args=(), daemon=True)
th2 = Thread(target=McuReceiver, args=(), daemon=True)
thConn = Thread(target=ConnectCamSocket)
thConn.start()
ConnectDevieceSocket()
ConnectMcuSocket()
th1.start()
th1.join()
th2.join()
