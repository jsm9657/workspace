#!/usr/bin/env python
"""\
Simple g-code streaming script for grbl
"""

import socket
import time
import sys
import binascii
import errno
import os
from threading import Thread

#HOST = '192.168.0.191'
HOST = '192.168.0.31'
PORT = 20000
BUFSIZE = 1024

HOSTCAM = '192.168.1.1'
#HOSTCAM = '192.168.1.195'
PORTCAM = 22000

#HOSTS = '192.168.10.36'
HOSTS = '192.168.1.1'
#HOSTS = '192.168.1.195'
PORTS = 21000

os.chdir('/home/pi/gcodefile')


class GlobalObject():
    sendMode = 'Nomal'
    status = 'READY'
    qq = False
    Msocketlife = False
    current_line = 0
    client = None  # mobile
    addr = None  # mobile
    clientCam = None  # camera
    addrCam = None  # camera
    Msocket = socket.socket()
    home = ['M29 Y', 'M29 X', 'M29 Z', 'M29 C', 'M29 B', 'G91 A15.', 'M29 A']
    homeFlag = 0
    Real_temp = False
    gcodeFile = None
    line = None
    statusCommand = ['M316', 'M356', 'M358', 'M359']
    statusCommandFlag = 0
    dispenserGetCommand = ['M311', 'M312',
                           'M313', 'M314', 'M315', 'M317', 'M318']
    dispenserGetCommandFlag = 0


def ConnectCamSocket():
    serverCam.listen()
    GlobalObject.clientCam, GlobalObject.addrCam = serverCam.accept()
    #GlobalObject.clientCam.settimeout(5)
    thZoom = Thread(target=CameraZoomValueReciver, args=(GlobalObject,))
    thZoom.start()


def CameraZoomValueReciver(GlobalObject):
    while True:
        try:
            temp = GlobalObject.clientCam.recv(4)
            if len(temp) == 0:
                raise Exception
            cameraValue = int.from_bytes(
                temp, byteorder='big', signed=True)
            if cameraValue == 10:
                print('auto on')
                camMessage = 'v4l2-ctl -d 2 -c focus_auto=0'
                os.system(camMessage)
                print('auto off')
                camMessage = 'v4l2-ctl -d 2 -c focus_auto=1'
                os.system(camMessage)
            elif cameraValue >= 100 and cameraValue <= 800:
                camMessage = 'v4l2-ctl -d 2 -c zoom_absolute=' + \
                    str(cameraValue)
                os.system(camMessage)
            elif cameraValue == 801:
                th10 = Thread(target=StartFFserver, args=())
                th10.start()
            elif cameraValue == 802:
                print('802sucssesdasd')
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
            elif cameraValue == 803:
                th12 = Thread(target=StartFFserver2, args=())
                th12.start()
            elif cameraValue == 804:
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
        except ConnectionError:
            GlobalObject.clientCam.close()
            thConn = Thread(target=ConnectCamSocket)
            print('연결해제')
            thConn.start()
            break
        except socket.error:
            GlobalObject.clientCam.close()
            thConn = Thread(target=ConnectCamSocket)
            print('타임아웃')
            thConn.start()
            break
        except Exception as e:
            GlobalObject.clientCam.close()
            thConn = Thread(target=ConnectCamSocket)
            print(e)
            thConn.start()
            break

def StartFFserver():
    EX = "ffserver -f ffserver.conf & ffmpeg -f v4l2 -s 640x480 -r 30 -i /dev/video0 http://localhost:8090/feed1.ffm"
    os.system(EX)
def FFserverKill():
    EX = 'pkill ffserver'
    os.system(EX)
def StartFFserver2():
    EX = "ffserver -f ffserver1.conf & ffmpeg -f v4l2 -s 1280x720 -r 16 -i /dev/video2 http://localhost:8092/feed2.ffm"
    os.system(EX)

def ConnectDevieceSocket():
    server.listen()
    GlobalObject.client, GlobalObject.addr = server.accept()
    print(GlobalObject.addr)
    GlobalObject.client.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 30)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 5)
    GlobalObject.client.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 3)

# Open grbl serial port


def ConnectMcuSocket():
    if not GlobalObject.Msocketlife:
        try:
            GlobalObject.Msocket.connect((HOST, PORT))
            GlobalObject.Msocketlife = True
            th2 = Thread(target=McuReceive, args=(GlobalObject,), daemon=True)
            th2.start()
        except:
            GlobalObject.client.send('MCU not connect\n'.encode())
            print('MCU not connect')
            GlobalObject.Msocketlife = False


def encoding(message):
    sendarray = bytearray()
    sendarray.append(0x1A)
    sendarray.append(0x20 + len(message))
    sendarray.extend(message.encode('utf-8'))
    sendarray.append(0x0A)
    return sendarray


def deviceReceive(GlobalObject):
    filename = 'temp'
    while True:
        try:
            temp = bytearray()
            while True:
                bit = GlobalObject.client.recv(1)
                temp.append(bit[0])
                if bit[0] == 0x0A:
                    break
            if GlobalObject.sendMode == 'Nomal':
                if temp.decode('ascii') == 'FileName\n':
                    filenameByte = bytearray()
                    while True:
                        bit = GlobalObject.client.recv(1)
                        filenameByte.append(bit[0])
                        if bit[0] == 0x0A:
                            break
                    filename = filenameByte.decode('ascii').strip()
                    print(temp.decode('ascii'))
                    print(filename)
                elif temp.decode('ascii') == 'START\n':
                    if filename == 'temp':
                        GlobalObject.client.send('no file\n'.encode())
                    else:
                        # 파일 존재여부 확인 필요
                        if os.path.isfile(filename) :
                            GlobalObject.sendMode = 'File'
                            GlobalObject.status = 'START'
                            th3 = Thread(target=GcodeFileSend,
                                        args=(GlobalObject, filename))
                            th3.start()
                        else : 
                            print("No File")
                            GlobalObject.client.send('no file\n'.encode())
                elif temp.decode('ascii') == 'HOME\n':
                    GlobalObject.sendMode = 'Home'
                    th4 = Thread(target=Home, args=(GlobalObject,))
                    th4.start()
                elif temp.decode('ascii') == 'STOP\n':
                    GlobalObject.Msocket.send(bytearray([3, 0x0A]))
                elif temp.decode('ascii') == 'PAUSE\n':
                    pass
                else:
                    print(temp.decode())
                    GlobalObject.Msocket.send(temp)
            elif GlobalObject.sendMode == 'File' or GlobalObject.sendMode == 'FileStatus' or GlobalObject.sendMode == 'FileDispenser':
                status = temp.decode('ascii')
                if status == 'START\n':
                    GlobalObject.status = 'START'
                    GlobalObject.Msocket.send(bytearray([2, 0x0A]))
                elif status == 'PAUSE\n':
                    GlobalObject.status = 'PAUSE'
                    GlobalObject.Msocket.send(bytearray([1, 0x0A]))
                elif status == 'STOP\n':
                    GlobalObject.status = 'STOP'
                    GlobalObject.Msocket.send(bytearray([3, 0x0A]))
                    GcodeFileEnd(GlobalObject)
            elif GlobalObject.sendMode == 'Home':
                try:
                    status = temp.decode('ascii')
                    if status == 'STOP\n':
                        GlobalObject.status = 'STOP'
                        GlobalObject.Msocket.send(bytearray([3, 0x0A]))
                        GlobalObject.sendMode = 'Nomal'
                except:
                    pass
        except ConnectionResetError:
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()
        except OSError:
            try:
                GlobalObject.client.send('retry please\n'.encode())
                print('retry please')
            except:
                GlobalObject.client.close()
                ConnectDevieceSocket()
                ConnectMcuSocket()
        except Exception:
            GlobalObject.client.close()
            ConnectDevieceSocket()
            ConnectMcuSocket()


def Home(GlobalObject):
    time.sleep(0.1)
    homecom = GlobalObject.home[GlobalObject.homeFlag]
    GlobalObject.qq = False
    GlobalObject.Msocket.send(encoding(homecom))
    GlobalObject.homeFlag += 1
    if(GlobalObject.homeFlag == len(GlobalObject.home)):
        GlobalObject.homeFlag = 0
        GlobalObject.sendMode = 'Nomal'
    print('Send home Mcommand')


def McuReceive(GlobalObject):
    while GlobalObject.Msocketlife:
        try:
            temp = bytearray()
            bit = GlobalObject.Msocket.recv(1)
            temp.append(bit[0])
            if bit[0] == 0x01:
                # dispenser 데이터 전송
                print('dispenser Data')
                bit = GlobalObject.Msocket.recv(2)
                temp.extend(bit)
                if temp[2] == 0x02:
                    bit = GlobalObject.Msocket.recv(4)
                    temp.extend(bit)
                    bit = GlobalObject.Msocket.recv(1)
                else:
                    bit = GlobalObject.Msocket.recv(temp[2]+1)
                    temp.extend(bit)
                    bit = GlobalObject.Msocket.recv(1)
                try:
                    GlobalObject.client.send(temp)
                    print('dispenser Data send Device')
                except:
                    print('Not open client socket')
            else:
                while True:
                    bit = GlobalObject.Msocket.recv(1)
                    temp.append(bit[0])
                    if bit[0] == 0x0A:
                        break
                try:
                    if temp.decode('ascii') == 'qq\n':
                        GlobalObject.qq = True
                        print(temp.decode('ascii'))
                        try:
                            if GlobalObject.sendMode == 'File':
                                sendline = 'line:%i\n' % GlobalObject.current_line
                                GlobalObject.client.send(sendline.encode())
                                th5 = Thread(target=sendLine,
                                             args=(GlobalObject,))
                                th5.start()
                            elif GlobalObject.sendMode == 'FileStatus':
                                th6 = Thread(target=sendStatus,
                                             args=(GlobalObject,))
                                th6.start()
                            elif GlobalObject.sendMode == 'FileDispenser':
                                th6 = Thread(target=sendDispenser,
                                             args=(GlobalObject,))
                                th6.start()
                            elif GlobalObject.sendMode == 'Home':
                                th4 = Thread(target=Home, args=(GlobalObject,))
                                th4.start()
                        except:
                            pass
                    elif temp.decode('ascii') == 'RR\n':
                        try:
                            if GlobalObject.sendMode == 'File':
                                th5 = Thread(target=reSendLine,
                                             args=(GlobalObject,))
                                th5.start()
                        except:
                            pass
                    else:
                        # print(temp)
                        pass
                except:
                    # print(temp)
                    pass
                try:
                    GlobalObject.client.send(temp)
                except:
                    print('Not open client socket')
        except socket.timeout:
            pass
        except socket.error:
            print('Gsocket error')
            GlobalObject.Msocket = socket.socket()
            try:
                GlobalObject.Msocket.connect((HOST, PORT))
            except:
                GlobalObject.client.send('except MCU\n'.encode())
                GlobalObject.Msocketlife = False
                break
            print('except MCU')


def sendLine(GlobalObject):
    if GlobalObject.gcodeFile is None:
        print('Not open file')
        return
    while True:
        GlobalObject.line = GlobalObject.gcodeFile.readline()
        # EOL 체크 확인 필요
        if not GlobalObject.line:
            GcodeFileEnd(GlobalObject)
            break
        else:
            l = GlobalObject.line.strip()
            if len(l) != 0 and l[0] != '#' and l[0] != ';' and l[0] != '(':  # real send line
                print(l)
                GlobalObject.qq = False
                # Send g-code block to grbl
                GlobalObject.Msocket.send(encoding(l))
                GlobalObject.client.send((l+'\n').encode())
                GlobalObject.current_line += 1
                print(GlobalObject.current_line)
                break
            elif l.startswith(';LAYER:'):
                GlobalObject.sendMode = 'FileStatus'
                th6 = Thread(target=sendStatus, args=(GlobalObject,))
                th6.start()
                break
            elif l == ';(*** end of start-gcode for Dr.Invivo 4D6)':
                GlobalObject.sendMode = 'FileDispenser'
                th6 = Thread(target=sendDispenser, args=(GlobalObject,))
                th6.start()
                break


def sendStatus(GlobalObject):
    statuscom = GlobalObject.statusCommand[GlobalObject.statusCommandFlag]
    GlobalObject.qq = False
    GlobalObject.Msocket.send(encoding(statuscom))
    GlobalObject.statusCommandFlag += 1
    if(GlobalObject.statusCommandFlag == len(GlobalObject.statusCommand)):
        GlobalObject.statusCommandFlag = 0
        GlobalObject.sendMode = 'File'
    print('Send Status Mcommand')


def sendDispenser(GlobalObject):
    dispensercom = GlobalObject.dispenserGetCommand[GlobalObject.dispenserGetCommandFlag]
    GlobalObject.qq = False
    GlobalObject.Msocket.send(encoding(dispensercom))
    GlobalObject.dispenserGetCommandFlag += 1
    if(GlobalObject.dispenserGetCommandFlag == len(GlobalObject.dispenserGetCommand)):
        GlobalObject.dispenserGetCommandFlag = 0
        GlobalObject.sendMode = 'File'
    print('Send Dispenser Mcommand')


def reSendLine(GlobalObject):
    l = GlobalObject.line.strip()
    GlobalObject.qq = False
    GlobalObject.Msocket.send(encoding(l))


def GcodeFileEnd(GlobalObject):
    GlobalObject.Msocket.send(encoding('M330'))
    GlobalObject.gcodeFile.close()
    GlobalObject.gcodeFile = None
    GlobalObject.line = None
    GlobalObject.current_line = 0
    GlobalObject.qq = False
    GlobalObject.statusCommandFlag = 0
    GlobalObject.dispenserGetCommandFlag = 0
    GlobalObject.status = 'READY'
    GlobalObject.sendMode = 'Nomal'


def GcodeFileSend(GlobalObject, gcodeFileName):
    time.sleep(2)
    # Open g-code file
    bomf = open(gcodeFileName, 'r', encoding='utf-8-sig').read()
    open(gcodeFileName, 'w', encoding='utf-8').write(bomf)
    GlobalObject.gcodeFile = open(gcodeFileName, 'r')
    th5 = Thread(target=sendLine, args=(GlobalObject,))
    th5.start()


server = socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((HOSTS, PORTS))

serverCam = socket.socket()
serverCam.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serverCam.bind((HOSTCAM, PORTCAM))

th1 = Thread(target=deviceReceive, args=(GlobalObject,), daemon=True)
th2 = Thread(target=McuReceive, args=(GlobalObject,), daemon=True)
thConn = Thread(target=ConnectCamSocket)
thConn.start()
ConnectDevieceSocket()
ConnectMcuSocket()
th1.start()
th1.join()
th2.join()
