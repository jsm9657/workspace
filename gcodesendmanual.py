#!/usr/bin/env python
"""\
Simple g-code streaming script for grbl
"""
 
import socket
import time
import sys
import binascii
import errno
import os
from threading import Thread
from gcodefunc import *

HOST = '192.168.0.191'
PORT = 20000
BUFSIZE = 1024

HOSTS = '192.168.1.1'
PORTS = 21000 

os.chdir('/home/pi/gcodefile')

class G():
    sendMode = 'Nomal'
    status = 'READY'
    qq = False
    socketlife = True
    current_line = 0
    client = None
    addr = None

# server = socket.socket()
# server.bind((HOSTS,PORTS))
# server.listen()

# G.client, G.addr = server.accept()

# Open grbl serial port
s = socket.socket() 
s.connect((HOST, PORT)) 

def deviceReceive(s, G):
    filename = 'temp'
    while True :
        try:
            temp = input('sendmessage : ').encode()
            if temp == 'e' :
                temp = input('sendencodemessage : ').encode()
                
            if G.sendMode == 'Nomal':
                if temp.decode('ascii') == 'FileName\n' :
                    filenameByte = bytearray()
                    filename = filenameByte.decode('utf-8').strip()
                    filename
                    print(temp.decode('ascii'))
                    print(filename)
                if temp.decode('ascii') == 'START\n' :
                    if filename == 'temp' :
                        print('no file\n')
                    else :
                        G.sendMode = 'File'
                        G.status = 'START'
                        th3 = Thread(target=GcodeFileSend,args=(s, G,filename))
                        th3.start()
                else :
                    s.send(temp)
            elif G.sendMode == 'File':
                status = temp.decode('ascii')
                if status == 'START\n' :
                    G.status = 'START'
                    s.send(bytearray([2,0x0A]))
                elif status == 'PAUSE\n' :
                    G.status ='PAUSE'
                    s.send(bytearray([1,0x0A]))        
                elif status == 'STOP\n' :
                    G.status ='STOP'
                    s.send(bytearray([3,0x0A]))
                # else :
                #      s.send(temp)
        except :
            pass
            # server.listen()
            # G.client, G.addr = server.accept()

def McuReceive(s, G):
    while True :
        temp = bytearray()
        while True :
            bit = s.recv(1)
            temp.append(bit[0])
            if bit[0] == 0x0A : break
        if temp.decode('ascii') == 'qq\n' :
            G.qq = True
            sendline = 'line:%i\n' % G.current_line
            try:
                G.client.send(sendline.encode())
            except :
                pass
        print(temp.decode('ascii'))
        try:
            G.client.send(temp)
        except:
            if G.socketlife :
                print('Not open client socket')
            else :
                break

def GcodeFileSend(s, G, gcodeFileName):
    drvon = 'M72'
    s.send(encoding(drvon))
    # Open g-code file
    bomf = open(gcodeFileName,'r',encoding='utf-8-sig').read()
    open(gcodeFileName,'w',encoding='utf-8').write(bomf)
    f = open(gcodeFileName,'r')
    # Wake up grbl
    #s.send("\r\n\r\n")
    #time.sleep(2)   # Wait for grbl to initialize
    #s.flushInput()  # Flush startup text in serial input
    # Stream g-code to grbl
    countr = 0
    breakflag = False
    for line in f:
        if breakflag:
            break 
        l = line.strip() # Strip all EOL characters for streaming
        if len(l) != 0 and l[0] != '#' and l[0] != ';' and l[0] != '(': #real send line
            print(l)
            G.qq = False
            s.send(encoding(l)) # Send g-code block to grbl
            G.current_line += 1
            print(G.current_line)
            while True: # Wait qq and Check Status
                if G.status == 'START':
                    if G.qq :
                        countr +=1
                        break
                elif G.status == 'PAUSE':
                    pass
                elif G.status == 'STOP':
                    breakflag = True
                    break
              
    # Wait here until grbl is finished to close serial port and file.
    print('line :',G.current_line )
    print('countqq :',countr)
    f.close()
    G.current_line = 0
    G.status = 'READY'
    G.sendMode = 'Nomal'


th1 = Thread(target=deviceReceive, args=(s,G))
th2 = Thread(target=McuReceive, args=(s,G))
th1.start()
th2.start()
th1.join()
th2.join()




# str = chr(27)+chr(14)
# s.send(encoding(str))
# temp = s.recv(BUFSIZE).decode('utf-8')
# while True:
#     drvon = 'M72'
#     s.send(encoding(drvon))
#     temp = s.recv(BUFSIZE).decode('utf-8')
#     print(temp)

#     # Open g-code file
#     gcodeFileName = '96WELL-4D6_200520 - 1CH.gcode'
#     bomf = open(gcodeFileName,'r',encoding='utf-8-sig').read()
#     open(gcodeFileName,'w',encoding='utf-8').write(bomf)
#     f = open(gcodeFileName,'r')
#     # Wake up grbl
#     #s.send("\r\n\r\n")
#     #time.sleep(2)   # Wait for grbl to initialize
#     #s.flushInput()  # Flush startup text in serial input

#     # Stream g-code to grbl
#     temp = s.recv(BUFSIZE).decode('utf-8')
#     print(':' + temp.strip())
#     countl = 0
#     countr = 0
#     for line in f:
#         l = line.strip() # Strip all EOL characters for streaming
#         if len(l) > 0 :
#             print(l)
#             s.send(encoding(l)) # Send g-code block to grbl
#             countl += 1
#             while True:
#                 temp = bytearray
#                 while True :
#                     a = s.recv(1)
#                     temp.extend(a)
#                     if a == 0x0A :
#                         break
#             print(temp.decode('ascii'))
#             if temp.decode('ascii') == 'qq\n':
#                 countr +=1
#                 break
#     while True:
#         try:
#             temp = s.recv(BUFSIZE).decode('utf-8')
#             print(temp)
#         except socket.timeout:
#             print('timeout')
#             break
#     # Wait here until grbl is finished to close serial port and file.
#     s.send(encoding('M73'))
#     print('line :',countl )
#     print('countqq :',countr)
#     input("  Press <Enter> to exit and disable grbl.")
 
# Close file and serial port
# f.close()
# s.close()
