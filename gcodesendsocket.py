#!/usr/bin/env python
'''\
Simple g-code streaming script for grbl
'''

import socket
import time
import sys
import binascii
import errno
import os
import logging
import logging.handlers
from omxplayer.player import OMXPlayer
from pathlib import Path
from threading import Thread

pythonVersion = '1.0.6'

# log setting
logger = logging.getLogger(__name__)
formatter = logging.Formatter(
    '[%(asctime)s]|[%(levelname)s|%(filename)s:%(lineno)s] >> %(message)s')
streamHandler = logging.StreamHandler()
#fileHandler = logging.FileHandler('/home/pi/gcodefile/log.log')
logFileMaxByte = 1024*1024*100
fileHandler = logging.handlers.RotatingFileHandler(
    '/home/pi/gcodefile/log.log', maxBytes=logFileMaxByte, backupCount=20)
streamHandler.setFormatter(formatter)
fileHandler.setFormatter(formatter)

logger.addHandler(streamHandler)
logger.addHandler(fileHandler)
logger.setLevel(level=logging.DEBUG)
logger.info('pythonVersion : ' + pythonVersion)
# cam path
campath1 = ''
campath2 = ''
try:
    camlist = os.listdir('/dev/v4l/by-id')
    for campath in camlist:
        if 'usb-HD_Camera_Manufacturer_USB_2.0_Camera' in campath and 'index0' in campath:
            campath1 = campath
        if 'usb-e-con_systems_See3CAM' in campath and 'index0' in campath:
            campath2 = campath
except:
    pass
if campath1 == '' or campath2 == '':
    logger.error('cam path error')
else:
    logger.info('cam path normal')

# MCU socket ip, port
#HOST = '192.168.1.145'
HOST = '192.168.0.191'
PORT = 20000
BUFSIZE = 1024

# Cam socket ip port
HOSTCAM = '192.168.1.1'
PORTCAM = 22000

# Device socket ip port
HOSTS = '192.168.1.1'
PORTS = 21000

# 작업 공간 변경
os.chdir('/home/pi/gcodefile')
gcodedelete = 'sudo rm *.gcode'
os.system(gcodedelete)
# os.system('sudo amixer -c 0 cset numid=3 1')

# 상수 정의
CONST_SENDMODE_NOMAL = 'NOMAL'
CONST_SENDMODE_HOME = 'HOME'
CONST_SENDMODE_FILE = 'FILE'

CONST_SENDFILE_FILELINE = 'FILELINE'
CONST_SENDFILE_FILESTATUS = 'FILESTATUS'

CONST_STATUS_READY = 'READY'
CONST_STATUS_START = 'START'
CONST_STATUS_PAUSE = 'PAUSE'
CONST_STATUS_STOP = 'STOP'

# Global 변수 클래스


class GlobalObject():
    # 현재 보내기 모드(NOMAL FILE FILEDISPENSER FILESTATUS HOME)
    send_mode = CONST_SENDMODE_NOMAL
    alarm_flag = False
    send_file_mode = CONST_SENDFILE_FILELINE
    status = CONST_STATUS_READY  # 현재 출력 상태(READY는 맨 처음 START PAUSE STOP)
    mcu_socket_life = False  # 재 접속시 MCU 소켓이 살아있는지 확인
    client = None  # mobile
    addr = None  # mobile
    client_cam = None  # camera
    addr_cam = None  # camera
    mcu_socket = socket.socket()
    gcode_file = None  # g code file path
    gcode_file_name = 'NoFile'
    gcode_file_totla_line = 0
    current_gcodefile_command = None  # 현재 gcode file 커맨드
    current_line = 0  # 현재 gcode file line 번호
    home_command = ['M29 Z', 'M29 C', 'G0 B15. F300',
                    'M29 B', 'M29 Y', 'M29 X', 'G79', 'M29 A']
    home_flag = 0  # home command index
    home_recive_flag = False
    status_command = ['M316', 'M356']
    status_command_flag = 0  # status command index
    video_player = None
    current_dispenser_status = [bytearray(), bytearray(), bytearray(
    ), bytearray(), bytearray(), bytearray(), bytearray()]
    current_door_status = True
    current_light_status = True
    video_path = 'opening.mp4'
    data_flag = False
    qq_flag = False
    send_line_client_flag = True
    wait_sendmessage = 0.001


def video():
    time.sleep(1)
    GlobalObject.video_player = OMXPlayer(
        GlobalObject.video_path, args=['--loop', '--no-osd'])
    time.sleep(13)
    if GlobalObject.video_path == 'opening.mp4':
        videoload(8)


def videoload(num):
    if num == 1:
        GlobalObject.video_path = 'opening.mp4'
    elif num == 2:
        GlobalObject.video_path = 'operating.mp4'
    elif num == 3:
        GlobalObject.video_path = 'pause.mp4'
    elif num == 4:
        GlobalObject.video_path = 'stop.mp4'
    elif num == 5:
        GlobalObject.video_path = 'door_open.mp4'
        # os.system('aplay door_open_sound.wav')
    elif num == 6:
        GlobalObject.video_path = 'door_close.mp4'
        # os.system('aplay door_close_sound.wav')
    elif num == 7:
        GlobalObject.video_path = 'ending.mp4'
    elif num == 8:
        GlobalObject.video_path = 'standard.mp4'
    else:
        GlobalObject.video_path = 'standard.mp4'
    if num == 7:
        GlobalObject.video_player.quit()
        GlobalObject.video_player = OMXPlayer(
            GlobalObject.video_path, args=['--no-osd'])
    else:
        GlobalObject.video_player.load(GlobalObject.video_path)

# cam socket server listen


def ConnectCamSocket():
    serverCam = socket.socket()
    serverCam.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverCam.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    serverCam.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 5)
    serverCam.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 3)
    serverCam.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 3)
    serverCam.bind((HOSTCAM, PORTCAM))
    serverCam.listen()
    logger.info('camlisten')
    GlobalObject.client_cam, GlobalObject.addr_cam = serverCam.accept()
    serverCam.close()
    thZoom = Thread(target=CameraValueReciver, args=(), daemon=True)
    thZoom.start()

# Cam 포커스 및 줌 server on off


def CameraValueReciver():
    while True:
        try:
            temp = GlobalObject.client_cam.recv(4)
            if len(temp) == 0:
                ResetCamSocket()
                break
            cameraValue = int.from_bytes(
                temp, byteorder='big', signed=True)
            if cameraValue == 10:
                logger.info('auto on')
                camMessage = 'v4l2-ctl -d /dev/v4l/by-id/'+campath2+' -c focus_auto=0'
                os.system(camMessage)
                logger.info('auto off')
                camMessage = 'v4l2-ctl -d /dev/v4l/by-id/'+campath2+' -c focus_auto=1'
                os.system(camMessage)
            elif cameraValue >= 100 and cameraValue <= 800:
                camMessage = 'v4l2-ctl -d /dev/v4l/by-id/'+campath2+' -c zoom_absolute=' + \
                    str(cameraValue)
                os.system(camMessage)
            elif cameraValue == 801:
                th10 = Thread(target=StartFFserver, args=())
                th10.start()
            elif cameraValue == 802:
                logger.info('802sucssesdasd')
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
            elif cameraValue == 803:
                th12 = Thread(target=StartFFserver2, args=())
                th12.start()
            elif cameraValue == 804:
                th11 = Thread(target=FFserverKill, args=())
                th11.start()
        except socket.error:
            logger.error('camera socket error out')
            ResetCamSocket()
            break
        except Exception:
            logger.error('camera Exception Error')
            break


def ResetCamSocket():
    GlobalObject.client_cam.close()
    GlobalObject.client.close()
    thConn = Thread(target=ConnectCamSocket, daemon=True)
    thConn.start()
    th11 = Thread(target=FFserverKill, args=())
    th11.start()


def StartFFserver():
    EX = 'ffserver -f ffserver.conf & ffmpeg -f v4l2 -s 640x480 -r 30 -i /dev/v4l/by-id/' + \
        campath1+' http://localhost:8090/feed1.ffm'
    os.system(EX)


def FFserverKill():
    EX = 'pkill ffserver'
    os.system(EX)


def StartFFserver2():
    EX = 'ffserver -f ffserver1.conf & ffmpeg -f v4l2 -s 1280x720 -r 16 -i /dev/v4l/by-id/' + \
        campath2+' http://localhost:8092/feed2.ffm'
    os.system(EX)


def ConnectDevieceSocket():
    server = socket.socket()
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    server.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 5)
    server.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 3)
    server.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 3)
    server.bind((HOSTS, PORTS))
    server.listen()
    logger.info('devicelisten')
    GlobalObject.client, GlobalObject.addr = server.accept()
    GlobalObject.client.settimeout(5)
    server.close()
    logger.info(GlobalObject.addr)
    GlobalObject.send_line_client_flag = True
    if GlobalObject.send_mode == CONST_SENDMODE_FILE:
        try:
            deviceMsgSender(
                ('FileStatus:'+GlobalObject.gcode_file_name+'/'+GlobalObject.gcode_file_totla_line+'\n').encode())
            for i in GlobalObject.current_dispenser_status:
                deviceMsgSender(i)
        except:
            pass


def ConnectMcuSocket():
    if not GlobalObject.mcu_socket_life:
        try:
            GlobalObject.mcu_socket.connect((HOST, PORT))
            GlobalObject.mcu_socket.setsockopt(
                socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            GlobalObject.mcu_socket_life = True
            th2 = Thread(target=McuReceiver, args=(), daemon=True)
            th2.start()
        except:
            try:
                deviceMsgSender('MCU not connect\n'.encode())
            except:
                pass
            logger.error('MCU not connect')
            GlobalObject.mcu_socket_life = False

# MUC 통신 프로토콜


def encoding(message):
    sendarray = bytearray()
    sendarray.append(0x1A)
    sendarray.append(0x20 + len(message))
    sendarray.extend(message.encode('ascii'))
    sendarray.append(0x0A)
    return sendarray


def DeviceReceiver():
    while True:
        try:
            temp = bytearray()
            while True:
                bit = GlobalObject.client.recv(1)
                if not bit:
                    logger.error('not_bit')
                    ResetSocket()
                    temp = bytearray()
                    continue
                temp.append(bit[0])
                if bit[0] == 0x0A:
                    break
            if GlobalObject.send_mode == CONST_SENDMODE_NOMAL:
                if temp.decode('ascii') == 'FileStatus\n':
                    filenameByte = bytearray()
                    while True:
                        bit = GlobalObject.client.recv(1)
                        filenameByte.append(bit[0])
                        if bit[0] == 0x0A:
                            break
                    filenameTotalline = filenameByte.decode(
                        'ascii').strip().split('/')
                    GlobalObject.gcode_file_name = filenameTotalline[0]
                    GlobalObject.gcode_file_totla_line = filenameTotalline[1]
                    logger.debug(temp.decode('ascii'))
                    logger.debug(GlobalObject.gcode_file_name)
                elif temp.decode('ascii') == 'START\n':
                    if GlobalObject.gcode_file_name == 'NoFile':
                        deviceMsgSender('no file\n'.encode())
                    else:
                        # 파일 존재여부 확인 필요
                        if os.path.isfile(GlobalObject.gcode_file_name):
                            GlobalObject.send_mode = CONST_SENDMODE_FILE
                            GlobalObject.status = CONST_STATUS_START
                            th3 = Thread(target=GcodeFileSender,
                                         args=())
                            th3.start()
                            videoload(2)
                        else:
                            logger.debug('No File')
                            deviceMsgSender('no file\n'.encode())
                elif temp.decode('ascii') == 'HOME\n':
                    GlobalObject.send_mode = CONST_SENDMODE_HOME
                    th4 = Thread(target=Home, args=())
                    th4.start()
                elif temp.decode('ascii') == 'STOP\n':
                    GlobalObject.mcu_socket.sendall(bytearray([3]))
                    logger.debug(GlobalObject.status + ':' +
                                 GlobalObject.send_mode + '1')
                    try:
                        GcodeFileEnd(False)
                    except Exception as e:
                        logger.debug(e)
                elif temp.decode('ascii') == 'PAUSE\n':
                    pass
                elif temp.decode('ascii') == 'RESET\n':
                    GlobalObject.gcode_file_name = 'NoFile'
                    GlobalObject.mcu_socket.sendall(bytearray([7]))
                    logger.debug('RESET')
                    GcodeFileEnd(False)
                elif temp.decode('ascii') == 'GOSTOP\n':
                    SendGoStop()
                elif 'Date:' in temp.decode('ascii'):
                    curdate = temp.decode('ascii').replace(
                        'Date:', '').replace('\n', '')
                    datesetmessage = 'sudo date '+curdate
                    try:
                        logger.info('set time')
                        os.system(datesetmessage)
                    except:
                        logger.error('do not set time')
                else:
                    if 'M76' in temp.decode('ascii'):
                        videoload(5)
                        GlobalObject.current_door_status = False
                    elif 'M77' in temp.decode('ascii'):
                        videoload(6)
                        GlobalObject.current_door_status = True
                    logger.debug(temp.decode('ascii'))
                    GlobalObject.mcu_socket.sendall(temp)
            elif GlobalObject.send_mode == CONST_SENDMODE_FILE:
                status = temp.decode('ascii')
                if status == 'START\n':
                    GlobalObject.status = CONST_STATUS_START
                    GlobalObject.mcu_socket.sendall(bytearray([2]))
                    videoload(2)
                elif status == 'PAUSE\n':
                    GlobalObject.status = CONST_STATUS_PAUSE
                    GlobalObject.mcu_socket.sendall(bytearray([1]))
                    videoload(3)
                elif status == 'STOP\n':
                    GlobalObject.status = CONST_STATUS_STOP
                    GlobalObject.send_mode = CONST_SENDMODE_NOMAL
                    GlobalObject.mcu_socket.sendall(bytearray([3]))
                    logger.debug(GlobalObject.status + ':' +
                                 GlobalObject.send_mode + '3')
                    videoload(4)
                    GcodeFileEnd()
                elif status == 'RESET\n':
                    GlobalObject.gcode_file_name = 'NoFile'
                    GlobalObject.mcu_socket.sendall(bytearray([7]))
                    logger.debug('RESET')
                    GcodeFileEnd(False)
            elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                try:
                    status = temp.decode('ascii')
                    if status == 'STOP\n':
                        GlobalObject.status = CONST_STATUS_STOP
                        GlobalObject.mcu_socket.sendall(bytearray([3]))
                        GlobalObject.home_flag = 0
                        GlobalObject.home_recive_flag = False
                        GlobalObject.send_mode = CONST_SENDMODE_NOMAL
                except:
                    pass
        except socket.timeout:
            continue
        except socket.error:
            logger.error('device socket.error')
            ResetSocket()
        except:
            logger.error('Device Except')


def ResetSocket():
    GlobalObject.client.close()
    ConnectDevieceSocket()
    ConnectMcuSocket()


def SendGoStop():
    try:
        GlobalObject.mcu_socket.sendall(encoding(chr(27)+'j'))
        # time.sleep(0.01)
        # GlobalObject.mcu_socket.sendall(encoding('G90'))
        logger.debug('send stopmessage')
    except:
        logger.debug('not send stopmessage')


def Home():
    if(GlobalObject.home_flag == len(GlobalObject.home_command)):
        GlobalObject.home_flag = 0
        GlobalObject.send_mode = CONST_SENDMODE_NOMAL
        deviceMsgSender('EndHome\n'.encode())
    else:
        homecom = GlobalObject.home_command[GlobalObject.home_flag]
        if 'M29' in homecom:
            GlobalObject.home_recive_flag = True
        GlobalObject.qq_flag = True
        GlobalObject.mcu_socket.sendall(encoding(homecom))
        GlobalObject.home_flag += 1
        logger.debug('Send home Mcommand')


def McuReceiver():
    while GlobalObject.mcu_socket_life:
        try:
            temp = bytearray()
            bit = GlobalObject.mcu_socket.recv(1)
            if not bit:
                raise socket.error
            temp.append(bit[0])
            if bit[0] == 0x01 or bit[0] == 0x0F:
                # dispenser 데이터 전송
                logger.debug('dispenser Data')
                for _ in range(2):
                    bit = GlobalObject.mcu_socket.recv(1)
                    if not bit:
                        raise socket.error
                    temp.extend(bit)
                for _ in range(temp[2]+1):
                    bit = GlobalObject.mcu_socket.recv(1)
                    if not bit:
                        raise socket.error
                    temp.extend(bit)
                try:
                    if(temp[4] == 0x31):
                        GlobalObject.current_dispenser_status[0] = temp
                    elif(temp[4] == 0x32):
                        GlobalObject.current_dispenser_status[1] = temp
                    elif(temp[4] == 0x33):
                        GlobalObject.current_dispenser_status[2] = temp
                    elif(temp[4] == 0x34):
                        GlobalObject.current_dispenser_status[3] = temp
                    elif(temp[4] == 0x35):
                        GlobalObject.current_dispenser_status[4] = temp
                    elif(temp[4] == 0x37):
                        GlobalObject.current_dispenser_status[5] = temp
                    elif(temp[4] == 0x38):
                        GlobalObject.current_dispenser_status[6] = temp
                    deviceMsgSender(temp)
                    logger.debug('dispenser Data send Device')
                except:
                    pass
                    # logger.debug('Not open client socket')
                recevieData()
            elif bit[0] == 0x03:
                # dispenser 데이터 전송
                logger.debug('incubator Data')
                for _ in range(2):
                    bit = GlobalObject.mcu_socket.recv(1)
                    if not bit:
                        raise socket.error
                    temp.extend(bit)
                for _ in range(temp[2]+2):
                    bit = GlobalObject.mcu_socket.recv(1)
                    if not bit:
                        raise socket.error
                    temp.extend(bit)
                try:
                    deviceMsgSender(temp)
                    logger.debug('incubator Data send Device')
                except:
                    pass
                    # logger.debug('Not open client socket')
                recevieData()
            else:
                while True:
                    bit = GlobalObject.mcu_socket.recv(1)
                    temp.append(bit[0])
                    if bit[0] == 0x0A:
                        break
                try:
                    if temp.decode('ascii') == 'qq\n':
                        logger.debug(temp.decode('ascii'))
                        recevieQQ()
                    elif temp.decode('ascii') == 'RR\n':
                        try:
                            logger.debug(temp.decode('ascii'))
                            if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                                th5 = Thread(target=reSendLine,
                                             args=())
                                th5.start()
                        except:
                            pass
                    elif '*err' in temp.decode('ascii'):
                        errnum = temp.decode('ascii').replace(
                            '*err ', '').replace('\n', '')
                        if errnum == '22':
                            pass
                        elif errnum == '23':
                            pass
                    elif '*hend' in temp.decode('ascii'):
                        receivehend()
                    elif '*stop' in temp.decode('ascii'):
                        if GlobalObject.send_mode == CONST_SENDMODE_NOMAL:
                            # GlobalObject.mcu_socket.sendall(bytearray([3]))
                            logger.debug(GlobalObject.status + ':' +
                                         GlobalObject.send_mode + '1')
                            try:
                                GcodeFileEnd(False)
                            except Exception as e:
                                logger.debug(e)
                        elif GlobalObject.send_mode == CONST_SENDMODE_FILE:
                            GlobalObject.status = CONST_STATUS_STOP
                            GlobalObject.send_mode = CONST_SENDMODE_NOMAL
                            # GlobalObject.mcu_socket.sendall(bytearray([3]))
                            logger.debug(GlobalObject.status + ':' +
                                         GlobalObject.send_mode + '3')
                            videoload(4)
                            GcodeFileEnd()
                        elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                            GlobalObject.status = CONST_STATUS_STOP
                            # GlobalObject.mcu_socket.sendall(bytearray([3]))
                            GlobalObject.home_flag = 0
                            GlobalObject.home_recive_flag = False
                            GlobalObject.send_mode = CONST_SENDMODE_NOMAL
                    elif '*start' in temp.decode('ascii'):
                        if GlobalObject.send_mode == CONST_SENDMODE_NOMAL:
                            if GlobalObject.gcode_file_name == 'NoFile':
                                deviceMsgSender('no file\n'.encode())
                            else:
                                if os.path.isfile(GlobalObject.gcode_file_name):
                                    GlobalObject.send_mode = CONST_SENDMODE_FILE
                                    GlobalObject.status = CONST_STATUS_START
                                    th3 = Thread(target=GcodeFileSender,
                                                 args=())
                                    th3.start()
                                    videoload(2)
                                else:
                                    logger.debug('No File')
                                    deviceMsgSender('no file\n'.encode())
                    elif '*pause' in temp.decode('ascii'):
                        if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                            GlobalObject.status = CONST_STATUS_PAUSE
                            # GlobalObject.mcu_socket.sendall(bytearray([1]))
                            videoload(3)
                    elif '*resume' in temp.decode('ascii'):
                        if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                            GlobalObject.status = CONST_STATUS_START
                            # GlobalObject.mcu_socket.sendall(bytearray([2]))
                            videoload(2)
                    elif '*tc' in temp.decode('ascii'):
                        touchcommand = temp.decode('ascii').replace('\n', '')
                        if touchcommand == '*tc 1 1':
                            if GlobalObject.current_light_status:
                                GlobalObject.current_light_status = False
                            else:
                                GlobalObject.current_light_status = True
                        elif touchcommand == '*tc 3 1':
                            if GlobalObject.current_door_status:
                                videoload(5)
                                GlobalObject.current_door_status = False
                            else:
                                videoload(6)
                                GlobalObject.current_door_status = True
                    else:
                        pass
                except:
                    pass
                try:
                    if '*' in temp.decode('ascii'):
                        logger.debug(temp.decode('ascii'))
                    deviceMsgSender(temp)
                except:
                    pass
                    # logger.debug('Not open client socket')
        except socket.timeout:
            pass
        except socket.error:
            logger.debug('Mcu socket error')
            GlobalObject.mcu_socket = socket.socket()
            try:
                GlobalObject.mcu_socket.connect((HOST, PORT))
            except:
                deviceMsgSender('except MCU\n'.encode())
                GlobalObject.mcu_socket_life = False
                break
            logger.debug('except MCU')
        except:
            GlobalObject.client.sendall('except MCU\n'.encode())
            GlobalObject.mcu_socket_life = False
            break


def shutdownAlarm():
    if GlobalObject.send_mode == CONST_SENDMODE_FILE or GlobalObject.send_mode == CONST_SENDMODE_HOME:
        GlobalObject.alarm_flag = True
    else:
        if not GlobalObject.alarm_flag:
            GlobalObject.alarm_flag = True
            th99 = Thread(target=sendShutdownAlarm, args=())
            th99.start()


def sendShutdownAlarm():
    try:
        GlobalObject.mcu_socket.sendall(encoding('M459'))
        GlobalObject.alarm_flag = False
        logger.debug('ShutDown Alarm')
    except:
        GlobalObject.alarm_flag = False


def receivehend():
    if GlobalObject.send_mode == CONST_SENDMODE_HOME:
        GlobalObject.home_recive_flag = False


def recevieQQ():
    GlobalObject.qq_flag = False
    if not GlobalObject.data_flag:
        try:
            if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                if GlobalObject.alarm_flag:
                    th99 = Thread(target=sendShutdownAlarm, args=())
                    th99.start()
                else:
                    if GlobalObject.send_file_mode == CONST_SENDFILE_FILELINE:
                        sendline = 'line:%i\n' % GlobalObject.current_line
                        try:
                            deviceMsgSender(sendline.encode())
                            pass
                        except:
                            pass
                        th5 = Thread(target=sendLine,
                                     args=())
                        th5.start()
                    elif GlobalObject.send_file_mode == CONST_SENDFILE_FILESTATUS:
                        th6 = Thread(target=sendStatus,
                                     args=())
                        th6.start()
            elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                pass
                if GlobalObject.alarm_flag:
                    th99 = Thread(target=sendShutdownAlarm, args=())
                    th99.start()
                else:
                    if not GlobalObject.home_recive_flag:
                        th4 = Thread(target=Home, args=())
                        th4.start()
        except:
            pass


def recevieData():
    if GlobalObject.qq_flag:
        GlobalObject.data_flag = False
    else:
        if GlobalObject.data_flag:
            GlobalObject.data_flag = False
            try:
                if GlobalObject.send_mode == CONST_SENDMODE_FILE:
                    if GlobalObject.alarm_flag:
                        th99 = Thread(target=sendShutdownAlarm, args=())
                        th99.start()
                    else:
                        if GlobalObject.send_file_mode == CONST_SENDFILE_FILELINE:
                            sendline = 'line:%i\n' % GlobalObject.current_line
                            try:
                                deviceMsgSender(sendline.encode())
                                pass
                            except:
                                pass
                            th5 = Thread(target=sendLine,
                                         args=())
                            th5.start()
                        elif GlobalObject.send_file_mode == CONST_SENDFILE_FILESTATUS:
                            th6 = Thread(target=sendStatus,
                                         args=())
                            th6.start()
                elif GlobalObject.send_mode == CONST_SENDMODE_HOME:
                    if GlobalObject.alarm_flag:
                        th99 = Thread(target=sendShutdownAlarm, args=())
                        th99.start()
                    else:
                        th4 = Thread(target=Home, args=())
                        th4.start()
            except:
                pass


def sendLine():
    if GlobalObject.gcode_file is None:
        logger.debug('Not open file')
        return
    while True:
        GlobalObject.current_gcodefile_command = GlobalObject.gcode_file.readline()
        # EOL 체크 확인 필요
        if not GlobalObject.current_gcodefile_command:
            GcodeFileEnd(False)
            videoload(8)
            break
        else:
            raw_command = GlobalObject.current_gcodefile_command.strip()
            # real send line
            if len(raw_command) != 0 and raw_command[0] != '#' and raw_command[0] != ';' and raw_command[0] != '(':
                send_command_line = raw_command.split(';')[0].strip()
                logger.debug(send_command_line)
                checkDataCommend(send_command_line)
                if 'M308' in send_command_line:
                    GlobalObject.send_file_mode = CONST_SENDFILE_FILESTATUS
                GlobalObject.qq_flag = True
                
                # Send g-code block to grbl
                try:
                    time.sleep(GlobalObject.wait_sendmessage)
                    GlobalObject.wait_sendmessage = 0.001
                    GlobalObject.mcu_socket.sendall(
                        encoding(send_command_line))
                except:
                    logger.debug('fail send msessage')
                
                checkUVCommend(send_command_line)
                deviceMsgSender((send_command_line+'\n').encode())
                GlobalObject.current_line += 1
                logger.debug(GlobalObject.current_line)
                break
            elif raw_command.startswith(';LAYER:'):
                GlobalObject.send_file_mode = CONST_SENDFILE_FILESTATUS
                th6 = Thread(target=sendStatus, args=())
                th6.start()
                break


def checkDataCommend(msg):
    data_command_list = ['M300', 'M303', 'M304', 'M305', 'M306', 'M307', 'M308', 'M309',
                         'M310', 'M311', 'M312', 'M313', 'M314', 'M315', 'M317', 'M318', 'M319']
    for i in data_command_list:
        if(i in msg):
            GlobalObject.data_flag = True
            return
    GlobalObject.data_flag = False
    return


def checkUVCommend(msg):
    uv_command_list = ['M172', 'M380', 'M381', 'M385',
                       'M383', 'M386', 'M384', 'M390', 'M171']
    for i in uv_command_list:
        if(i in msg):
            GlobalObject.wait_sendmessage = 0.05
            return
    return


def sendStatus():
    try:
        statuscom = GlobalObject.status_command[GlobalObject.status_command_flag]
        GlobalObject.status_command_flag += 1
        if(GlobalObject.status_command_flag == len(GlobalObject.status_command)):
            GlobalObject.status_command_flag = 0
            GlobalObject.send_file_mode = CONST_SENDFILE_FILELINE
        checkDataCommend(statuscom)
        GlobalObject.qq_flag = True
        GlobalObject.mcu_socket.sendall(encoding(statuscom))
        deviceMsgSender((statuscom+'\n').encode())
        logger.debug('Send Status Mcommand : '+statuscom)
    except:
        pass


def reSendLine():
    l = GlobalObject.current_gcodefile_command.strip()
    GlobalObject.qq_flag = False
    try:
        GlobalObject.mcu_socket.sendall(encoding(l))
    except:
        logger.debug('fail send message')


def GcodeFileEnd(stop = True):
    if stop :
        try:
            GlobalObject.mcu_socket.sendall(encoding('G0 Z40. C30. B0.'))
        except:
            logger.debug('fail send z40 c30 b0')
        logger.debug('send z40 c30 b0')
    else :
        pass
    try:
        deviceMsgSender('EndPrint\n'.encode())
    except:
        pass
    try:
        GlobalObject.gcode_file.close()
    except:
        pass
    GlobalObject.gcode_file = None
    GlobalObject.current_gcodefile_command = None
    GlobalObject.current_line = 0
    GlobalObject.status_command_flag = 0
    GlobalObject.dispenser_get_command_flag = 0
    GlobalObject.status = CONST_STATUS_READY
    GlobalObject.send_mode = CONST_SENDMODE_NOMAL
    GlobalObject.send_file_mode = CONST_SENDFILE_FILELINE


def GcodeFileSender():
    time.sleep(2)
    # Open g-code file
    bomf = open(GlobalObject.gcode_file_name, 'r', encoding='utf-8-sig').read()
    open(GlobalObject.gcode_file_name, 'w', encoding='utf-8').write(bomf)
    GlobalObject.gcode_file = open(GlobalObject.gcode_file_name, 'r')
    th5 = Thread(target=sendLine, args=())
    th5.start()


def deviceMsgSender(msg):
    try:
        GlobalObject.client.sendall(msg, socket.MSG_DONTWAIT)
    except Exception as e:
        logger.error(e)
        GlobalObject.client.close()

# def ServerAlive():
#     while True:
#         serveralive.listen()
#         clientalive, addralive = serveralive.accept()
#         clientalive.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
#         clientalive.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 60)
#         clientalive.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 5)
#         clientalive.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 3)
#         logger.debug('alivesocket')
#         try:
#             alivedata = clientalive.recv(1)
#             if not alivedata:
#                 raise Exception
#             else :
#                 raise Exception
#         except :
#             try:
#                 GlobalObject.client.close()
#             except :
#                 pass


# init
# serveralive = socket.socket()
# serveralive.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# serveralive.bind((HOSTS, 24000))
# th999 = Thread(target=ServerAlive, args=(), daemon=True)
# th999.start()

# lcd video player
th3 = Thread(target=video, args=(), daemon=True)
th3.start()
# camera socket
thConn = Thread(target=ConnectCamSocket)
thConn.start()
# mobile device socket server thread
th1 = Thread(target=DeviceReceiver, args=(), daemon=True)
ConnectDevieceSocket()
# Mcu board socket
ConnectMcuSocket()
# start main
th1.start()
th1.join()
